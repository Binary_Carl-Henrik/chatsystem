﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Receiver
{
    public class Receiver
    {
        private const int listenPort = 11000;

        private static void StartReceiver(AllMessages messages)
        {
            bool done = false;

            UdpClient listener = new UdpClient(listenPort);
            IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);
            try
            {
                while (!done)
                {
                    Console.WriteLine("Waiting for broadcast");
                    byte[] bytes = listener.Receive(ref groupEP);
                    messages.putMessage(bytes);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                listener.Close();
            }
        }

        public static int Main()
        {
            AllMessages messages = new AllMessages();
            ConsolePrinter printer = new ConsolePrinter();
            new Thread(() => printer.Start(messages)).Start();
            StartReceiver(messages);

            return 0;
        }

        private static string getMessage(byte[] bytes)
        {
            string message = "";
            Message temp = new Message();
            string[] messageParts = Encoding.ASCII.GetString(bytes, 0, bytes.Length).Split('|');
            bool fail = false;
            foreach(var part in messageParts)
            {
                string[] data = part.Split(':');
                if(data.Length == 2)
                {
                    switch (data[0])
                    {
                        case "fromAddress":
                            temp._address = data[1];
                            break;
                        case "time":
                            temp._time = data[1];
                            break;
                        case "message":
                            temp._message = data[1];
                            break;
                        default:
                            Console.WriteLine("FEEL");
                            break;
                    }
                }
                else
                {
                    fail = true;
                }
            }
            if (!fail)
            {
                message = "From: " + temp._address + "\n";
                message += "Time: " + temp._time + "\n";
                message += "Message: " + temp._message;
                return message;

            }
            return message;
        }
    }

    class Message
    {
        public string _message { set; get; }
        public string _time { set; get; }
        public string _address { set; get; }
        public Message()
        {

        }

        public Message(string message, string time, string address)
        {
            _message = message;
            _time = time;
            _address = address;
        }
    }
}
