﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Receiver
{
    class ConsolePrinter
    {
        public ConsolePrinter()
        {

        }

        public void Start(AllMessages messages)
        {
            while (true)
            {
                string[] temp = messages.getAllMessagesToString();
                Console.Clear();
                foreach (var message in temp)
                {
                    Console.WriteLine(message);
                }
            }
        }
    }
}
