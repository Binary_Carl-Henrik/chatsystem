﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Receiver
{
    class AllMessages
    {
        private List<Message> _messages;
        public readonly object _mutex;
        public bool newMessage { private set; get; }

        public AllMessages()
        {
            _messages = new List<Message>();
            _mutex = new object();
            newMessage = false;
        }

        public string[] getAllMessagesToString()
        {
            List<string> allMessages = new List<string>();
            lock (_mutex)
            {
                if (!newMessage) { Monitor.Wait(_mutex); }
                foreach(var message in _messages)
                {
                    allMessages.Add(getMessage(message));
                }
            }
            newMessage = false;
            return allMessages.ToArray();
        }

        private string getMessage(Message _message)
        {
            string message = "";
            message = "From: " + _message._address + "\n";
            message += "Time: " + _message._time + "\n";
            message += "Message: " + _message._message;
            return message;
        }

        public void putMessage(Message message)
        {
            lock (_mutex)
            {
                _messages.Add(message);
                newMessage = true;
                Monitor.PulseAll(_mutex);
            }
        }

        private Message createMessage(byte[] bytes)
        {
            Message temp = new Message();
            string[] messageParts = Encoding.ASCII.GetString(bytes, 0, bytes.Length).Split('|');
            foreach (var part in messageParts)
            {
                string[] data = part.Split(':');
                if (data.Length == 2)
                {
                    switch (data[0])
                    {
                        case "fromAddress":
                            temp._address = data[1];
                            break;
                        case "time":
                            temp._time = data[1];
                            break;
                        case "message":
                            temp._message = data[1];
                            break;
                        default:
                            Console.WriteLine("FEEL");
                            break;
                    }
                }
            }
            return temp;
        }

        public void putMessage(byte[] bytes)
        {
            Message temp = createMessage(bytes);
            lock (_mutex)
            {
                _messages.Add(temp);
                newMessage = true;
                Monitor.PulseAll(_mutex);
            }
        }
    }
}
