﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace ChatClient
{
    class Program
    {
        static void Main(string[] args)
        {
            SenderBuffer buffer = new SenderBuffer();
            Sender send = new Sender();
            new Thread(() => send.Start(buffer)).Start();
            bool test = true;
            while (test)
            {
                Console.WriteLine("New message to send:");
                string message = Console.ReadLine();
                if (message.Equals("stop"))
                {
                    test = false;
                }
                else
                {
                    string time = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                    string address = "192.168.1.255";
                    Message temp = new Message(message, time, address);
                    buffer.putMessage(temp);
                }
            }

        }
    }
}
