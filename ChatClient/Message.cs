﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatClient
{
    class Message
    {
        public string _message { private set; get; }
        public string _time { private set; get; }
        public string _address { private set; get; }
        public Message()
        {

        }

        public Message(string message, string time, string address)
        {
            _message = message;
            _time = time;
            _address = address;
        }

        
    }
}
