﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ChatClient
{
    class SenderBuffer
    {
        private List<Message> _bufferMessage;
        private readonly object _mutexMessage;
        public SenderBuffer()
        {
            _mutexMessage = new object();
            _bufferMessage = new List<Message>();
        }

        public void putMessage(Message message)
        {
            lock (_mutexMessage)
            {
                _bufferMessage.Add(message);
                Monitor.PulseAll(_mutexMessage);
            }
        }

        public List<Message> getAllMessages()
        {
            lock (_mutexMessage)
            {
                while (_bufferMessage.Count == 0) { Monitor.Wait(_mutexMessage); }
                List<Message> result = new List<Message>(_bufferMessage.ToArray());
                _bufferMessage.Clear();
                return result;
            }
        }

        public Message getMessage()
        {
            return null;
        }
    }

    
}
