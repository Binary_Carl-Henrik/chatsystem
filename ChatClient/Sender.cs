﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ChatClient
{
    class Sender
    {
        public Sender()
        {

        }

        public void Start(SenderBuffer buffer)
        {
            while (true)
            {
                List<Message> messages = buffer.getAllMessages();
                if(messages.Count!= 0)
                {
                    foreach(var message in messages)
                    {
                        send(message);
                    }
                }
            }
        }

        private void send(Message message)
        {
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
            ProtocolType.Udp);
            IPAddress broadcast = IPAddress.Parse(message._address);

            byte[] sendbuf = Encoding.ASCII.GetBytes(formatMessage(message));
            IPEndPoint ep = new IPEndPoint(broadcast, 11000);
            s.SendTo(sendbuf, ep);
            Console.WriteLine("Message sent to the broadcast address");
        }

        private string formatMessage(Message message)
        {
            string toMessage = "";
            toMessage += "fromAddress:" + getIP() + "|";
            toMessage += "time:" + message._time + "|";
            toMessage += "message:" + message._message;
            return toMessage;
        }

        private string getIP()
        {
            string message = "unkown";
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach(var ip in host.AddressList)
            {
                Console.WriteLine("IP: " + ip.ToString());
                if(ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return message;
        }
    }
}
